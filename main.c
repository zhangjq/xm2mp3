
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdint.h>
#include <string.h>
#include <endian.h>

const uint8_t xm_magic[] = {'i', 'f', 'm', 't', ' ', 'M', 'P', '3'};
const uint8_t xm_unknown1[] = {0xFE, 0xFE, 0xFE, 0xFE};
const uint8_t xm_unknown2 = 0x00;

struct xm_header {
	uint8_t magic[8];
	uint8_t unknown1[4]; // unknown field
	uint16_t offset;
	uint8_t unknown2;
	uint8_t sum;
} __attribute__((__packed__));
_Static_assert(sizeof(struct xm_header) == 16, "sizeof .xm header should be 16");

struct xm_info {
	uint8_t *addr;
	size_t size;
	size_t decode_offset;
	unsigned int sum;
};

static int parse_xm_header(struct xm_info *xi)
{
	struct xm_header *xh = (struct xm_header *)xi->addr;
	if (memcmp(xh->magic, xm_magic, sizeof(xm_magic)) != 0) {
		fprintf(stderr, "magic mismatch\n");
		return -1;
	}
	if (memcmp(xh->unknown1, xm_unknown1, sizeof(xm_unknown1)) != 0) {
		fprintf(stderr, "unknown field1 mismatch\n");
		return -1;
	}
	if (xh->unknown2 != xm_unknown2) {
		fprintf(stderr, "unknown field2 mismatch\n");
		return -1;
	}
	size_t offset = le16toh(xh->offset);
	if (offset + sizeof(struct xm_header) >= xi->size) {
		fprintf(stderr, "offset overflow\n");
		return -1;
	}

	xi->decode_offset = offset;
	xi->sum = 0xFF + xh->sum;
	return 0;
}

static int load_ifile(const char *ifile, struct xm_info *xi)
{
	int fd = open(ifile, O_RDONLY);
	if (fd == -1) {
		perror("open");
		return -1;
	}

	struct stat s;
	if (fstat(fd, &s) == -1) {
		perror("fstat");
		close(fd);
		return -1;
	}
	xi->size = s.st_size;
	if (xi->size <= sizeof(struct xm_header)) {
		fprintf(stderr, "Invalid xm file: too small\n");
		close(fd);
		return -1;
	}

	xi->addr = mmap(NULL, xi->size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (xi->addr == MAP_FAILED) {
		perror("mmap");
		close(fd);
		return -1;
	}
	close(fd);

	if (parse_xm_header(xi) != 0) {
		fprintf(stderr, "Invalid xm file %s\n", ifile);
		munmap(xi->addr, xi->size);
		return -1;
	}
	return 0;
}

static void unload_ifile(const struct xm_info *xi)
{
	munmap(xi->addr, xi->size);
}

static int save_ofile(const char *ofile, const struct xm_info *xi)
{
	FILE *stream = fopen(ofile, "wx");
	if (!stream) {
		perror("fopen");
		return -1;
	}

	size_t off = sizeof(struct xm_header);
	if (xi->decode_offset)
		fwrite(xi->addr + off, xi->decode_offset, 1, stream);
	for (off += xi->decode_offset; off < xi->size; off++)
		fputc(xi->sum - xi->addr[off], stream);
	fclose(stream);
	return 0;
}

static int convert(const char *ifile, const char *ofile)
{
	struct xm_info xi;
	if (load_ifile(ifile, &xi) != 0) {
		fprintf(stderr, "Failed to load input file %s\n", ifile);
		return -1;
	}

	int ret = 0;
	if (save_ofile(ofile, &xi) != 0) {
		fprintf(stderr, "Failed to save to output file %s\n", ofile);
		ret = -1;
	}
	unload_ifile(&xi);
	return ret;
}

int main(int argc, char *argv[]) {
	if (argc != 3) {
		fprintf(stderr, "Usage: %s <input .xm file> <output .mp3 file>\n", argv[0]);
		return -1;
	}

	return convert(argv[1], argv[2]);
}
