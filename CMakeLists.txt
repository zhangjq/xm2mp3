cmake_minimum_required(VERSION 2.6)
project(xm2mp3)

set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_C_STANDARD 11)

add_executable(xm2mp3 main.c)

install(TARGETS xm2mp3 RUNTIME DESTINATION bin)
